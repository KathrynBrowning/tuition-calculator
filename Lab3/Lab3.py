# Program Name:         Lab3.py
# Course:               IT1113/Section W01
# Student Name:         Kathryn Browning
# Assignment Number:    Lab3
# Due Date:             03/4/2018

#This program displays the projected semester tuition for the years 2018 - 2024 and displays the results.
def main():
    input('\nIn 2017, the tuition for a full time student is $6,450 per semester.\nThe tuition will be going up for the next 7 years at a rate of 3.5% per year.\n\nTo see the projected semester tuition for the next 7 years, press ENTER.')

    tuition = 6450
    increase_percent = 0.035
    for x in range (2018, 2025):
        tuition_increase = tuition * increase_percent
        tuition += tuition_increase
        print ('\nThe projected tuition is ${:0.2f} for year '.format(tuition) + str(x))

# Call main function
main()
